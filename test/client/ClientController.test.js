/* eslint-disable arrow-parens */
/* eslint-disable no-undef */
const request = require('supertest');

jest.mock('pg');

const app = require('../../src/app');

const Client = require('../../src/model/Client');
const ClientController = require('../../src/controller/ClientController');
const QueryResponse = require('../../src/model/QueryResponse');
const ApiResponse = require('../../src/model/ApiResponse');

jest.mock('../../src/service/ClientService');

const _app = request(app);

const clientController = ClientController;

const client = new Client({
  id: 1,
  name: 'Cristian',
  email: 'cl@cl.cl',
  birthdate: '2021-01-01',
  license: 'A1',
  status: 'ACTIVE',
});

describe('CLIENT CONTROLLER TEST', () => {
  describe('GET ALL CLIENTS', () => {
    test('OK', async () => {
      clientController.clientService.all.mockResolvedValue(
        new QueryResponse({
          status: true,
          data: { clients: [client, client] },
        }),
      );

      await _app
        .get('/api/clients')
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              data: { clients: [client, client] },
            }),
          );
        });
    });

    test('ERROR', async () => {
      clientController.clientService.all.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .get('/api/clients')
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });

  describe('GET CLIENT BY ID', () => {
    test('OK', async () => {
      clientController.clientService.find.mockResolvedValue(
        new QueryResponse({
          status: true,
          data: { client },
        }),
      );

      await _app
        .get(`/api/clients/${client.id}`)
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              data: { client },
            }),
          );
        });
    });

    test('ERROR', async () => {
      clientController.clientService.find.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .get(`/api/clients/${client.id}`)
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });

  describe('STORE', () => {
    test('OK', async () => {
      clientController.clientService.store.mockResolvedValue(
        new QueryResponse({
          status: true,
          message: '',
          data: { client },
        }),
      );

      await _app
        .post('/api/clients')
        .send(client)
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              message: '',
              data: { client },
            }),
          );
        });
    });

    test('ERROR', async () => {
      clientController.clientService.store.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .post('/api/clients')
        .send(client)
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });

  describe('UPDATE', () => {
    test('OK', async () => {
      clientController.clientService.update.mockResolvedValue(
        new QueryResponse({
          status: true,
          message: '',
        }),
      );

      await _app
        .put(`/api/clients/${client.id}`)
        .send(client)
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              message: '',
            }),
          );
        });
    });

    test('ERROR', async () => {
      clientController.clientService.update.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .put(`/api/clients/${client.id}`)
        .send(client)
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });

  describe('DELETE', () => {
    test('OK', async () => {
      clientController.clientService.delete.mockResolvedValue(
        new QueryResponse({
          status: true,
          message: '',
        }),
      );

      await _app
        .delete(`/api/clients/${client.id}`)
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              message: '',
            }),
          );
        });
    });

    test('ERROR', async () => {
      clientController.clientService.delete.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .delete(`/api/clients/${client.id}`)
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });
});
