/* eslint-disable no-undef */
/* eslint-disable implicit-arrow-linebreak */
const Client = require('../../src/model/Client');
const ClientRepository = require('../../src/repository/ClientRepository');
const QueryResponse = require('../../src/model/QueryResponse');
const pool = require('../../db/pool');

jest.mock('../../src/utils/Utils');

const clientRepository = new ClientRepository();

const client = new Client({
  id: 1,
  name: 'Cristian',
  email: 'cl@cl.cl',
  birthdate: '2021-01-01',
  license: 'A1',
  status: 'ACTIVE',
});

afterAll(() => {
  pool.end();
});

describe('CLIENT REPOSITORY TEST', () => {
  describe('GET ALL CLIENTS', () => {
    test('OK', async () => {
      clientRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rows: [client, client] },
        }),
      );

      expect(await clientRepository.all()).toEqual(
        new QueryResponse({
          status: true,
          data: { clients: [client, client] },
          response: { rows: [client, client] },
        }),
      );
    });

    test('ERROR', async () => {
      clientRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
          response: {},
        }),
      );

      expect(await clientRepository.all()).toEqual(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
          response: {},
        }),
      );
    });
  });

  describe('GET BY ID', () => {
    test('OK', async () => {
      clientRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rows: [client], rowCount: 1 },
        }),
      );

      expect(await clientRepository.find(client.id)).toEqual(
        new QueryResponse({
          status: true,
          data: { client },
          response: { rows: [client], rowCount: 1 },
        }),
      );
    });

    test('ERROR', async () => {
      clientRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          response: {},
        }),
      );

      expect(await clientRepository.find(client.id)).toEqual(
        new QueryResponse({
          status: false,
          message: 'Cliente no existe',
          response: {},
        }),
      );
    });
  });

  describe('STORE', () => {
    test('OK', async () => {
      clientRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rows: [client], rowCount: 1 },
        }),
      );

      expect(await clientRepository.store(client)).toEqual(
        new QueryResponse({
          status: true,
          message: 'Cliente creado correctamente',
          data: { client },
          response: { rows: [client], rowCount: 1 },
        }),
      );
    });

    test('ERROR', async () => {
      clientRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          response: {},
        }),
      );

      expect(await clientRepository.store(client)).toEqual(
        new QueryResponse({
          status: false,
          message: 'Ocurrió un error al crear el cliente',
          response: {},
        }),
      );
    });
  });

  describe('UPDATE', () => {
    test('OK', async () => {
      clientRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rowCount: 1 },
        }),
      );

      expect(await clientRepository.update(client.id, client)).toEqual(
        new QueryResponse({
          status: true,
          message: 'Cliente actualizado correctamente',
          response: { rowCount: 1 },
        }),
      );
    });

    test('ERROR', async () => {
      clientRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          response: {},
        }),
      );

      expect(await clientRepository.update(client.id, client)).toEqual(
        new QueryResponse({
          status: false,
          message: 'Ocurrió un error al actualizar el cliente',
          response: {},
        }),
      );
    });
  });

  describe('DELETE', () => {
    test('OK', async () => {
      clientRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rowCount: 1 },
        }),
      );

      expect(await clientRepository.delete(client.id)).toEqual(
        new QueryResponse({
          status: true,
          message: 'Cliente eliminado correctamente',
          response: { rowCount: 1 },
        }),
      );
    });

    test('ERROR', async () => {
      clientRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          response: {},
        }),
      );

      expect(await clientRepository.delete(client.id)).toEqual(
        new QueryResponse({
          status: false,
          message: 'Ocurrió un error al eliminar el cliente',
          response: {},
        }),
      );
    });
  });
});
