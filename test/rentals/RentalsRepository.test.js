/* eslint-disable no-undef */
/* eslint-disable implicit-arrow-linebreak */
const Rentals = require('../../src/model/Rentals');
const RentalsRepository = require('../../src/repository/RentalsRepository');
const QueryResponse = require('../../src/model/QueryResponse');
const pool = require('../../db/pool');

jest.mock('../../src/utils/Utils');

const rentalsRepository = new RentalsRepository();

const rentals = new Rentals({
  id: 1,
  id_vehicle: 1,
  id_client: 1,
  id_price: 1,
  start_date: '27-11-2021',
  final_date: '27-11-2021',
  total: 20000,
  status: 'ACTIVE',
  observations: '',
});

afterAll(() => {
  pool.end();
});

describe('RENTALS REPOSITORY TEST', () => {
  describe('GET ALL RENTALS', () => {
    test('OK', async () => {
      rentalsRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rows: [rentals, rentals] },
        }),
      );

      expect(await rentalsRepository.all()).toEqual(
        new QueryResponse({
          status: true,
          data: { rentals: [rentals, rentals] },
          response: { rows: [rentals, rentals] },
        }),
      );
    });

    test('ERROR', async () => {
      rentalsRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
          response: {},
        }),
      );

      expect(await rentalsRepository.all()).toEqual(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
          response: {},
        }),
      );
    });
  });

  describe('GET BY ID', () => {
    test('OK', async () => {
      rentalsRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rows: [rentals], rowCount: 1 },
        }),
      );

      expect(await rentalsRepository.find(rentals.id)).toEqual(
        new QueryResponse({
          status: true,
          data: { rentals },
          response: { rows: [rentals], rowCount: 1 },
        }),
      );
    });

    test('ERROR', async () => {
      rentalsRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          response: {},
        }),
      );

      expect(await rentalsRepository.find(rentals.id)).toEqual(
        new QueryResponse({
          status: false,
          message: 'Alquiler no existe',
          response: {},
        }),
      );
    });
  });

  describe('STORE', () => {
    test('OK', async () => {
      rentalsRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rows: [rentals], rowCount: 1 },
        }),
      );

      expect(await rentalsRepository.store(rentals)).toEqual(
        new QueryResponse({
          status: true,
          message: 'Alquiler creado correctamente',
          data: { rentals },
          response: { rows: [rentals], rowCount: 1 },
        }),
      );
    });

    test('ERROR', async () => {
      rentalsRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          response: {},
        }),
      );

      expect(await rentalsRepository.store(rentals)).toEqual(
        new QueryResponse({
          status: false,
          message: 'Ocurrió un error al crear el alquiler',
          response: {},
        }),
      );
    });
  });

  describe('UPDATE', () => {
    test('OK', async () => {
      rentalsRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rowCount: 1 },
        }),
      );

      expect(await rentalsRepository.update(rentals.id, rentals)).toEqual(
        new QueryResponse({
          status: true,
          message: 'Alquiler actualizado correctamente',
          response: { rowCount: 1 },
        }),
      );
    });

    test('ERROR', async () => {
      rentalsRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: 'Ocurrió un error al actualizar el alquiler',
          response: {},
        }),
      );

      expect(await rentalsRepository.update(rentals.id, rentals)).toEqual(
        new QueryResponse({
          status: false,
          message: 'Ocurrió un error al actualizar el alquiler',
          response: {},
        }),
      );
    });
  });

  describe('DELETE', () => {
    test('OK', async () => {
      rentalsRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rowCount: 1 },
        }),
      );

      expect(await rentalsRepository.delete(rentals.id)).toEqual(
        new QueryResponse({
          status: true,
          message: 'Alquiler eliminado correctamente',
          response: { rowCount: 1 },
        }),
      );
    });

    test('ERROR', async () => {
      rentalsRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: 'Ocurrió un error al eliminar el alquiler',
          response: {},
        }),
      );

      expect(await rentalsRepository.delete(rentals.id)).toEqual(
        new QueryResponse({
          status: false,
          message: 'Ocurrió un error al eliminar el alquiler',
          response: {},
        }),
      );
    });
  });
});
