/* eslint-disable arrow-parens */
/* eslint-disable no-undef */
const request = require('supertest');

jest.mock('pg');

const app = require('../../src/app');

const Rentals = require('../../src/model/Rentals');
const RentalsController = require('../../src/controller/RentalsController');
const QueryResponse = require('../../src/model/QueryResponse');
const ApiResponse = require('../../src/model/ApiResponse');

jest.mock('../../src/service/RentalsService');

const _app = request(app);

const rentalsController = RentalsController;

const rentals = new Rentals({
  id: 1,
  id_vehicle: 1,
  id_client: 1,
  id_price: 1,
  start_date: '27-11-2021',
  final_date: '27-11-2021',
  total: 20000,
  status: 'ACTIVE',
  observations: '',
});

describe('RENTALS CONTROLLER TEST', () => {
  describe('GET ALL RENTALS', () => {
    test('OK', async () => {
      rentalsController.rentalsService.all.mockResolvedValue(
        new QueryResponse({
          status: true,
          data: { rentals: [rentals, rentals] },
        }),
      );

      await _app
        .get('/api/rentals')
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              data: { rentals: [rentals, rentals] },
            }),
          );
        });
    });

    test('ERROR', async () => {
      rentalsController.rentalsService.all.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .get('/api/rentals')
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });

  describe('GET RENTALS BY ID', () => {
    test('OK', async () => {
      rentalsController.rentalsService.find.mockResolvedValue(
        new QueryResponse({
          status: true,
          data: { rentals },
        }),
      );

      await _app
        .get(`/api/rentals/${rentals.id}`)
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              data: { rentals },
            }),
          );
        });
    });

    test('ERROR', async () => {
      rentalsController.rentalsService.find.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .get(`/api/rentals/${rentals.id}`)
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });

  describe('STORE', () => {
    test('OK', async () => {
      rentalsController.rentalsService.store.mockResolvedValue(
        new QueryResponse({
          status: true,
          message: '',
          data: { rentals },
        }),
      );

      await _app
        .post('/api/rentals')
        .send(rentals)
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              message: '',
              data: { rentals },
            }),
          );
        });
    });

    test('ERROR', async () => {
      rentalsController.rentalsService.store.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .post('/api/rentals')
        .send(rentals)
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });

  describe('UPDATE', () => {
    test('OK', async () => {
      rentalsController.rentalsService.update.mockResolvedValue(
        new QueryResponse({
          status: true,
          message: '',
        }),
      );

      await _app
        .put(`/api/rentals/${rentals.id}`)
        .send(rentals)
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              message: '',
            }),
          );
        });
    });

    test('ERROR', async () => {
      rentalsController.rentalsService.update.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .put(`/api/rentals/${rentals.id}`)
        .send(rentals)
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });

  describe('DELETE', () => {
    test('OK', async () => {
      rentalsController.rentalsService.delete.mockResolvedValue(
        new QueryResponse({
          status: true,
          message: '',
        }),
      );

      await _app
        .delete(`/api/rentals/${rentals.id}`)
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              message: '',
            }),
          );
        });
    });

    test('ERROR', async () => {
      rentalsController.rentalsService.delete.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .delete(`/api/rentals/${rentals.id}`)
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });
});
