/* eslint-disable no-undef */
/* eslint-disable implicit-arrow-linebreak */
const Price = require('../../src/model/Price');
const PriceRepository = require('../../src/repository/PriceRepository');
const QueryResponse = require('../../src/model/QueryResponse');
const pool = require('../../db/pool');

jest.mock('../../src/utils/Utils');

const priceRepository = new PriceRepository();

const price = new Price({
  id: 1,
  description: 'Prueba',
  day_value: '27-11-2021',
  vehicle_type: 'Carro',
  status: 'ACTIVE',
});

afterAll(() => {
  pool.end();
});

describe('PRICE REPOSITORY TEST', () => {
  describe('GET ALL PRICE', () => {
    test('OK', async () => {
      priceRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rows: [price, price] },
        }),
      );

      expect(await priceRepository.all()).toEqual(
        new QueryResponse({
          status: true,
          data: { prices: [price, price] },
          response: { rows: [price, price] },
        }),
      );
    });

    test('ERROR', async () => {
      priceRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
          response: {},
        }),
      );

      expect(await priceRepository.all()).toEqual(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
          response: {},
        }),
      );
    });
  });

  describe('GET BY ID', () => {
    test('OK', async () => {
      priceRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rows: [price], rowCount: 1 },
        }),
      );

      expect(await priceRepository.find(price.id)).toEqual(
        new QueryResponse({
          status: true,
          data: { price },
          response: { rows: [price], rowCount: 1 },
        }),
      );
    });

    test('ERROR', async () => {
      priceRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          response: {},
        }),
      );

      expect(await priceRepository.find(price.id)).toEqual(
        new QueryResponse({
          status: false,
          message: 'Precio no existe',
          response: {},
        }),
      );
    });
  });

  describe('STORE', () => {
    test('OK', async () => {
      priceRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rows: [price], rowCount: 1 },
        }),
      );

      expect(await priceRepository.store(price)).toEqual(
        new QueryResponse({
          status: true,
          message: 'Precio creado correctamente',
          data: { price },
          response: { rows: [price], rowCount: 1 },
        }),
      );
    });

    test('ERROR', async () => {
      priceRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          response: {},
        }),
      );

      expect(await priceRepository.store(price)).toEqual(
        new QueryResponse({
          status: false,
          message: 'Ocurrió un error al crear el precio',
          response: {},
        }),
      );
    });
  });

  describe('UPDATE', () => {
    test('OK', async () => {
      priceRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rowCount: 1 },
        }),
      );

      expect(await priceRepository.update(price.id, price)).toEqual(
        new QueryResponse({
          status: true,
          message: 'Precio actualizado correctamente',
          response: { rowCount: 1 },
        }),
      );
    });

    test('ERROR', async () => {
      priceRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          response: {},
        }),
      );

      expect(await priceRepository.update(price.id, price)).toEqual(
        new QueryResponse({
          status: false,
          message: 'Ocurrió un error al actualizar el precio',
          response: {},
        }),
      );
    });
  });

  describe('DELETE', () => {
    test('OK', async () => {
      priceRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rowCount: 1 },
        }),
      );

      expect(await priceRepository.delete(price.id)).toEqual(
        new QueryResponse({
          status: true,
          message: 'Precio eliminado correctamente',
          response: { rowCount: 1 },
        }),
      );
    });

    test('ERROR', async () => {
      priceRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          response: {},
        }),
      );

      expect(await priceRepository.delete(price.id)).toEqual(
        new QueryResponse({
          status: false,
          message: 'Ocurrió un error al eliminar el precio',
          response: {},
        }),
      );
    });
  });
});
