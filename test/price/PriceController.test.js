/* eslint-disable arrow-parens */
/* eslint-disable no-undef */
const request = require('supertest');

jest.mock('pg');

const app = require('../../src/app');

const Price = require('../../src/model/Price');
const PriceController = require('../../src/controller/PriceController');
const QueryResponse = require('../../src/model/QueryResponse');
const ApiResponse = require('../../src/model/ApiResponse');

jest.mock('../../src/service/PriceService');

const _app = request(app);

const priceController = PriceController;

const price = new Price({
  id: 1,
  description: 'Prueba',
  day_value: '27-11-2021',
  vehicle_type: 'Carro',
  status: 'ACTIVE',
});

describe('PRICE CONTROLLER TEST', () => {
  describe('GET ALL PRICE', () => {
    test('OK', async () => {
      priceController.priceService.all.mockResolvedValue(
        new QueryResponse({
          status: true,
          data: { prices: [price, price] },
        }),
      );

      await _app
        .get('/api/prices')
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              data: { prices: [price, price] },
            }),
          );
        });
    });

    test('ERROR', async () => {
      priceController.priceService.all.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .get('/api/prices')
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });

  describe('GET PRICE BY ID', () => {
    test('OK', async () => {
      priceController.priceService.find.mockResolvedValue(
        new QueryResponse({
          status: true,
          data: { price },
        }),
      );

      await _app
        .get(`/api/prices/${price.id}`)
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              data: { price },
            }),
          );
        });
    });

    test('ERROR', async () => {
      priceController.priceService.find.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .get(`/api/prices/${price.id}`)
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });

  describe('STORE', () => {
    test('OK', async () => {
      priceController.priceService.store.mockResolvedValue(
        new QueryResponse({
          status: true,
          message: '',
          data: { price },
        }),
      );

      await _app
        .post('/api/prices')
        .send(price)
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              message: '',
              data: { price },
            }),
          );
        });
    });

    test('ERROR', async () => {
      priceController.priceService.store.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .post('/api/prices')
        .send(price)
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });

  describe('UPDATE', () => {
    test('OK', async () => {
      priceController.priceService.update.mockResolvedValue(
        new QueryResponse({
          status: true,
          message: '',
        }),
      );

      await _app
        .put(`/api/prices/${price.id}`)
        .send(price)
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              message: '',
            }),
          );
        });
    });

    test('ERROR', async () => {
      priceController.priceService.update.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .put(`/api/prices/${price.id}`)
        .send(price)
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });

  describe('DELETE', () => {
    test('OK', async () => {
      priceController.priceService.delete.mockResolvedValue(
        new QueryResponse({
          status: true,
          message: '',
        }),
      );

      await _app
        .delete(`/api/prices/${price.id}`)
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              message: '',
            }),
          );
        });
    });

    test('ERROR', async () => {
      priceController.priceService.delete.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .delete(`/api/prices/${price.id}`)
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });
});
