/* eslint-disable no-undef */
/* eslint-disable implicit-arrow-linebreak */
const Vehicle = require('../../src/model/Vehicle');
const VehicleRepository = require('../../src/repository/VehicleRepository');
const QueryResponse = require('../../src/model/QueryResponse');
const pool = require('../../db/pool');

jest.mock('../../src/utils/Utils');

const vehicleRepository = new VehicleRepository();

const vehicle = new Vehicle({
  id: 1,
  type: 'Carro',
  model: '2020',
  cylinder_capacity: '1000',
  colour: 'Negro',
  plate: 'HWD234',
  mileage: '16000',
  status: 'ACTIVE',
  observations: '',
});

afterAll(() => {
  pool.end();
});

describe('VEHICLE REPOSITORY TEST', () => {
  describe('GET ALL VEHICLE', () => {
    test('OK', async () => {
      vehicleRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rows: [vehicle, vehicle] },
        }),
      );

      expect(await vehicleRepository.all()).toEqual(
        new QueryResponse({
          status: true,
          data: { vehicles: [vehicle, vehicle] },
          response: { rows: [vehicle, vehicle] },
        }),
      );
    });

    test('ERROR', async () => {
      vehicleRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
          response: {},
        }),
      );

      expect(await vehicleRepository.all()).toEqual(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
          response: {},
        }),
      );
    });
  });

  describe('GET BY ID', () => {
    test('OK', async () => {
      vehicleRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rows: [vehicle], rowCount: 1 },
        }),
      );

      expect(await vehicleRepository.find(vehicle.id)).toEqual(
        new QueryResponse({
          status: true,
          data: { vehicle },
          response: { rows: [vehicle], rowCount: 1 },
        }),
      );
    });

    test('ERROR', async () => {
      vehicleRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          response: {},
        }),
      );

      expect(await vehicleRepository.find(vehicle.id)).toEqual(
        new QueryResponse({
          status: false,
          message: 'Vehículo no existe',
          response: {},
        }),
      );
    });
  });

  describe('STORE', () => {
    test('OK', async () => {
      vehicleRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rows: [vehicle], rowCount: 1 },
        }),
      );

      expect(await vehicleRepository.store(vehicle)).toEqual(
        new QueryResponse({
          status: true,
          message: 'Vehículo creado correctamente',
          data: { vehicle },
          response: { rows: [vehicle], rowCount: 1 },
        }),
      );
    });

    test('ERROR', async () => {
      vehicleRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          response: {},
        }),
      );

      expect(await vehicleRepository.store(vehicle)).toEqual(
        new QueryResponse({
          status: false,
          message: 'Ocurrió un error al crear el vehículo',
          response: {},
        }),
      );
    });
  });

  describe('UPDATE', () => {
    test('OK', async () => {
      vehicleRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rowCount: 1 },
        }),
      );

      expect(await vehicleRepository.update(vehicle.id, vehicle)).toEqual(
        new QueryResponse({
          status: true,
          message: 'Vehículo actualizado correctamente',
          response: { rowCount: 1 },
        }),
      );
    });

    test('ERROR', async () => {
      vehicleRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: 'Ocurrió un error al actualizar el vehículo',
          response: {},
        }),
      );

      expect(await vehicleRepository.update(vehicle.id, vehicle)).toEqual(
        new QueryResponse({
          status: false,
          message: 'Ocurrió un error al actualizar el vehículo',
          response: {},
        }),
      );
    });
  });

  describe('DELETE', () => {
    test('OK', async () => {
      vehicleRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: true,
          response: { rowCount: 1 },
        }),
      );

      expect(await vehicleRepository.delete(vehicle.id)).toEqual(
        new QueryResponse({
          status: true,
          message: 'Vehículo eliminado correctamente',
          response: { rowCount: 1 },
        }),
      );
    });

    test('ERROR', async () => {
      vehicleRepository.utils.query.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: 'Ocurrió un error al eliminar el vehículo',
          response: {},
        }),
      );

      expect(await vehicleRepository.delete(vehicle.id)).toEqual(
        new QueryResponse({
          status: false,
          message: 'Ocurrió un error al eliminar el vehículo',
          response: {},
        }),
      );
    });
  });
});
