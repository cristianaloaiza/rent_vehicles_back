/* eslint-disable arrow-parens */
/* eslint-disable no-undef */
const request = require('supertest');

jest.mock('pg');

const app = require('../../src/app');

const Vehicle = require('../../src/model/Vehicle');
const VehicleController = require('../../src/controller/VehicleController');
const QueryResponse = require('../../src/model/QueryResponse');
const ApiResponse = require('../../src/model/ApiResponse');

jest.mock('../../src/service/VehicleService');

const _app = request(app);

const vehicleController = VehicleController;

const vehicles = new Vehicle({
  id: 1,
  type: 'Carro',
  model: '2020',
  cylinder_capacity: '1000',
  colour: 'Negro',
  plate: 'HWD234',
  mileage: '16000',
  status: 'ACTIVE',
  observations: '',
});

describe('VEHICLE CONTROLLER TEST', () => {
  describe('GET ALL VEHICLE', () => {
    test('OK', async () => {
      vehicleController.vehicleService.all.mockResolvedValue(
        new QueryResponse({
          status: true,
          data: { vehicles: [vehicles, vehicles] },
        }),
      );

      await _app
        .get('/api/vehicles')
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              data: { vehicles: [vehicles, vehicles] },
            }),
          );
        });
    });

    test('ERROR', async () => {
      vehicleController.vehicleService.all.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .get('/api/vehicles')
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });

  describe('GET VEHICLES BY ID', () => {
    test('OK', async () => {
      vehicleController.vehicleService.find.mockResolvedValue(
        new QueryResponse({
          status: true,
          data: { vehicles },
        }),
      );

      await _app
        .get(`/api/vehicles/${vehicles.id}`)
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              data: { vehicles },
            }),
          );
        });
    });

    test('ERROR', async () => {
      vehicleController.vehicleService.find.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .get(`/api/vehicles/${vehicles.id}`)
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });

  describe('STORE', () => {
    test('OK', async () => {
      vehicleController.vehicleService.store.mockResolvedValue(
        new QueryResponse({
          status: true,
          message: '',
          data: { vehicles },
        }),
      );

      await _app
        .post('/api/vehicles')
        .send(vehicles)
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              message: '',
              data: { vehicles },
            }),
          );
        });
    });

    test('ERROR', async () => {
      vehicleController.vehicleService.store.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .post('/api/vehicles')
        .send(vehicles)
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });

  describe('UPDATE', () => {
    test('OK', async () => {
      vehicleController.vehicleService.update.mockResolvedValue(
        new QueryResponse({
          status: true,
          message: '',
        }),
      );

      await _app
        .put(`/api/vehicles/${vehicles.id}`)
        .send(vehicles)
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              message: '',
            }),
          );
        });
    });

    test('ERROR', async () => {
      vehicleController.vehicleService.update.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .put(`/api/vehicles/${vehicles.id}`)
        .send(vehicles)
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });

  describe('DELETE', () => {
    test('OK', async () => {
      vehicleController.vehicleService.delete.mockResolvedValue(
        new QueryResponse({
          status: true,
          message: '',
        }),
      );

      await _app
        .delete(`/api/vehicles/${vehicles.id}`)
        .expect(200)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: true,
              message: '',
            }),
          );
        });
    });

    test('ERROR', async () => {
      vehicleController.vehicleService.delete.mockResolvedValue(
        new QueryResponse({
          status: false,
          message: '',
          error: {},
        }),
      );

      await _app
        .delete(`/api/vehicles/${vehicles.id}`)
        .expect(400)
        .then(response => {
          expect(response.body).toEqual(
            new ApiResponse({
              status: false,
              message: '',
              error: {},
            }),
          );
        });
    });
  });
});
