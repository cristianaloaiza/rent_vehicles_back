const ClientService = require('../service/ClientService');
const Utils = require('../utils/Utils');

const clientService = new ClientService();

/**
 * @api {get} /clients 1) Clients List
 * @apiName GetClients
 * @apiGroup Clients
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {Object} data
 * @apiSuccess {Object[]} data.clients
 * @apiSuccess {Number} data.clients.id
 * @apiSuccess {String} data.clients.document_number
 * @apiSuccess {String} data.clients.name
 * @apiSuccess {String} data.clients.email
 * @apiSuccess {String} data.clients.birthdate
 * @apiSuccess {String} data.clients.license
 * @apiSuccess {String} data.clients.status
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function index(_, res) {
  const response = await clientService.all();

  Utils.executeResponse(res, response);
}

/**
 * @api {get} /clients/:cliendId 2) Get Client By ID
 * @apiName GetClient
 * @apiGroup Clients
 *
 * @apiParam {Number} cliendId
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {Object} data
 * @apiSuccess {Object} data.client
 * @apiSuccess {Number} data.client.id
 * @apiSuccess {String} data.client.document_number
 * @apiSuccess {String} data.client.name
 * @apiSuccess {String} data.client.email
 * @apiSuccess {String} data.client.birthdate
 * @apiSuccess {String} data.client.license
 * @apiSuccess {String} data.client.status
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function find({ params: { clientId } }, res) {
  const response = await clientService.find(clientId);

  Utils.executeResponse(res, response);
}

/**
 * @api {post} /clients 3) Create New Client
 * @apiName CreateClient
 * @apiGroup Clients
 *
 * @apiBody {String} document_number
 * @apiBody {String} name
 * @apiBody {String} email
 * @apiBody {Date} birthdate="2021-01-01"
 * @apiBody {String} license
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {String} message
 * @apiSuccess {Object} data
 * @apiSuccess {Object} data.client
 * @apiSuccess {Number} data.client.id
 * @apiSuccess {String} data.client.document_number
 * @apiSuccess {String} data.client.name
 * @apiSuccess {String} data.client.email
 * @apiSuccess {String} data.client.birthdate
 * @apiSuccess {String} data.client.license
 * @apiSuccess {String} data.client.status
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function store({ body }, res) {
  const response = await clientService.store({ ...body, status: 'ACTIVE' });

  Utils.executeResponse(res, response);
}

/**
 * @api {put} /clients/:clientId 4) Update Client
 * @apiName UpdateClient
 * @apiGroup Clients
 *
 * @apiParam {Number} clientId
 *
 * @apiBody {String} document_number
 * @apiBody {String} name
 * @apiBody {String} email
 * @apiBody {Date} birthdate="2021-01-01"
 * @apiBody {String} license
 * @apiBody {String} status
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {String} message
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function update({ body, params: { clientId } }, res) {
  const response = await clientService.update(clientId, body);

  Utils.executeResponse(res, response);
}

/**
 * @api {delete} /clients/:cliendId 5) Delete Client By ID
 * @apiName DeleteClient
 * @apiGroup Clients
 *
 * @apiParam {Number} cliendId Client ID.
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {Striing} message
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function destroy({ params: { clientId } }, res) {
  const response = await clientService.delete(clientId);

  Utils.executeResponse(res, response);
}

module.exports = {
  clientService,
  find,
  index,
  store,
  update,
  destroy,
};
