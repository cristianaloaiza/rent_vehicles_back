const RentalsService = require('../service/RentalsService');
const Utils = require('../utils/Utils');

const rentalsService = new RentalsService();

/**
 * @api {get} /Rentals 1) Rentals List
 * @apiName GetRentals
 * @apiGroup Rentals
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {String} message
 * @apiSuccess {Object} data
 * @apiSuccess {Object} data.rentals
 * @apiSuccess {Number} data.rentals.id
 * @apiSuccess {Number} data.rentals.id_vehicle
 * @apiSuccess {Number} data.rentals.id_client
 * @apiSuccess {Number} data.rentals.id_price
 * @apiSuccess {String} data.rentals.start_date
 * @apiSuccess {String} data.rentals.final_date
 * @apiSuccess {String} data.rentals.total
 * @apiSuccess {String} data.rentals.status
 * @apiSuccess {String} data.rentals.observations
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function index(_, res) {
  const response = await rentalsService.all();

  Utils.executeResponse(res, response);
}

/**
 * @api {get} /Rentals/:rentalsId 2) Get Rentals By ID
 * @apiName GetRental
 * @apiGroup Rentals
 *
 * @apiParam {Number} rentalsId
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {String} message
 * @apiSuccess {Object} data
 * @apiSuccess {Object} data.rentals
 * @apiSuccess {Number} data.rentals.id
 * @apiSuccess {Number} data.rentals.id_vehicle
 * @apiSuccess {Number} data.rentals.id_client
 * @apiSuccess {Number} data.rentals.id_price
 * @apiSuccess {String} data.rentals.start_date
 * @apiSuccess {String} data.rentals.final_date
 * @apiSuccess {String} data.rentals.total
 * @apiSuccess {String} data.rentals.status
 * @apiSuccess {String} data.rentals.observations
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function find({ params: { rentalsId } }, res) {
  const response = await rentalsService.find(rentalsId);

  Utils.executeResponse(res, response);
}

/**
 * @api {post} /rentals 3) Create New Rentals
 * @apiName CreateRental
 * @apiGroup Rentals
 *
 * @apiBody {Number} id_vehicle
 * @apiBody {Number} id_client
 * @apiBody {Number} id_price
 * @apiBody {Date} start_date="2021-01-01"
 * @apiBody {Date} final_date="2021-01-01"
 * @apiBody {String} total
 * @apiBody {String} status
 * @apiBody {String} observations
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {String} message
 * @apiSuccess {Object} data
 * @apiSuccess {Object} data.rentals
 * @apiSuccess {Number} data.rentals.id
 * @apiSuccess {Number} data.rentals.id_vehicle
 * @apiSuccess {Number} data.rentals.id_client
 * @apiSuccess {Number} data.rentals.id_price
 * @apiSuccess {String} data.rentals.start_date
 * @apiSuccess {String} data.rentals.final_date
 * @apiSuccess {String} data.rentals.total
 * @apiSuccess {String} data.rentals.status
 * @apiSuccess {String} data.rentals.observations
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function store({ body }, res) {
  const response = await rentalsService.store({ ...body, status: 'ACTIVE' });

  Utils.executeResponse(res, response);
}

/**
 * @api {put} /rentals/:rentalsId 4) Update Rentals
 * @apiName UpdateRentals
 * @apiGroup Rentals
 *
 * @apiParam {Number} rentalsId
 *
 * @apiBody {Number} id_vehicle
 * @apiBody {Number} id_client
 * @apiBody {Number} id_price
 * @apiBody {String} start_date
 * @apiBody {String} final_date
 * @apiBody {String} total
 * @apiBody {String} status
 * @apiBody {String} observations
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {String} message
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function update({ body, params: { rentalsId } }, res) {
  const response = await rentalsService.update(rentalsId, body);

  Utils.executeResponse(res, response);
}

/**
 * @api {delete} /rentals/:rentalsId 5) Delete Rentals By ID
 * @apiName DeleteRentals
 * @apiGroup Rentals
 *
 * @apiParam {Number} rentalsId Rentals ID.
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {Striing} message
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function destroy({ params: { rentalsId } }, res) {
  const response = await rentalsService.delete(rentalsId);

  Utils.executeResponse(res, response);
}

module.exports = {
  rentalsService,
  find,
  index,
  store,
  update,
  destroy,
};
