const PriceService = require('../service/PriceService');
const Utils = require('../utils/Utils');

const priceService = new PriceService();

/**
 * @api {get} /prices 1) Prices List
 * @apiName GetPrices
 * @apiGroup Prices
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {String} message
 * @apiSuccess {Object} data
 * @apiSuccess {Object} data.price
 * @apiSuccess {Number} data.price.id
 * @apiSuccess {String} data.price.description
 * @apiSuccess {Double} data.price.day_value
 * @apiSuccess {String} data.price.vehicle_type
 * @apiSuccess {String} data.price.status
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function index(_, res) {
  const response = await priceService.all();

  Utils.executeResponse(res, response);
}

/**
 * @api {get} /prices/:priceId 2) Get Price By ID
 * @apiName GetPrice
 * @apiGroup Prices
 *
 * @apiParam {Number} priceId
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {String} message
 * @apiSuccess {Object} data
 * @apiSuccess {Object} data.price
 * @apiSuccess {Number} data.price.id
 * @apiSuccess {String} data.price.description
 * @apiSuccess {Double} data.price.day_value
 * @apiSuccess {String} data.price.vehicle_type
 * @apiSuccess {String} data.price.status
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function find({ params: { priceId } }, res) {
  const response = await priceService.find(priceId);

  Utils.executeResponse(res, response);
}

/**
 * @api {post} /prices 3) Create New Price
 * @apiName CreatePrice
 * @apiGroup Prices
 *
 * @apiBody {String} description
 * @apiBody {String} day_value
 * @apiBody {String} vehicle_type
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {String} message
 * @apiSuccess {Object} data
 * @apiSuccess {Object} data.price
 * @apiSuccess {Number} data.price.id
 * @apiSuccess {String} data.price.description
 * @apiSuccess {Double} data.price.day_value
 * @apiSuccess {String} data.price.vehicle_type
 * @apiSuccess {String} data.price.status
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function store({ body }, res) {
  const response = await priceService.store({ ...body, status: 'ACTIVE' });

  Utils.executeResponse(res, response);
}

/**
 * @api {put} /prices/:priceId 4) Update Price
 * @apiName UpdatePrice
 * @apiGroup Prices
 *
 * @apiParam {Number} priceId
 *
 * @apiBody {String} description
 * @apiBody {String} day_value
 * @apiBody {String} vehicle_type
 * @apiBody {String} status
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {String} message
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function update({ body, params: { priceId } }, res) {
  const response = await priceService.update(priceId, body);

  Utils.executeResponse(res, response);
}

/**
 * @api {delete} /prices/:priceId 5) Delete Price By ID
 * @apiName DeletePrice
 * @apiGroup Prices
 *
  * @apiParam {Number} priceId Price ID.
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {Striing} message
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function destroy({ params: { priceId } }, res) {
  const response = await priceService.delete(priceId);

  Utils.executeResponse(res, response);
}

module.exports = {
  priceService,
  find,
  index,
  store,
  update,
  destroy,
};
