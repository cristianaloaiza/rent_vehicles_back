const VehicleService = require('../service/VehicleService');
const Utils = require('../utils/Utils');

const vehicleService = new VehicleService();

/**
 * @api {get} /vehicles 1) Vehicles List
 * @apiName GetVehicles
 * @apiGroup Vehicles
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {String} message
 * @apiSuccess {Object} data
 * @apiSuccess {Object} data.vehicle
 * @apiSuccess {Number} data.vehicle.id
 * @apiSuccess {String} data.vehicle.type
 * @apiSuccess {String} data.vehicle.model
 * @apiSuccess {String} data.vehicle.cylinder_capacity
 * @apiSuccess {String} data.vehicle.colour
 * @apiSuccess {String} data.vehicle.plate
 * @apiSuccess {String} data.vehicle.mileage
 * @apiSuccess {String} data.vehicle.status
 * @apiSuccess {String} data.vehicle.observations
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function index(_, res) {
  const response = await vehicleService.all();

  Utils.executeResponse(res, response);
}

/**
 * @api {get} /vehicles/:vehicleId 2) Get Vehicle By ID
 * @apiName GetVehicle
 * @apiGroup Vehicles
 *
 * @apiParam {Number} vehicleId
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {String} message
 * @apiSuccess {Object} data
 * @apiSuccess {Object} data.vehicle
 * @apiSuccess {Number} data.vehicle.id
 * @apiSuccess {String} data.vehicle.type
 * @apiSuccess {String} data.vehicle.model
 * @apiSuccess {String} data.vehicle.cylinder_capacity
 * @apiSuccess {String} data.vehicle.colour
 * @apiSuccess {String} data.vehicle.plate
 * @apiSuccess {String} data.vehicle.mileage
 * @apiSuccess {String} data.vehicle.status
 * @apiSuccess {String} data.vehicle.observations
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function find({ params: { vehicleId } }, res) {
  const response = await vehicleService.find(vehicleId);

  Utils.executeResponse(res, response);
}

/**
 * @api {post} /vehicles 3) Create New Vehicle
 * @apiName CreateVehicle
 * @apiGroup Vehicles
 *
 * @apiBody {String} type
 * @apiBody {String} model
 * @apiBody {String} cylinder_capacity
 * @apiBody {String} colour
 * @apiBody {String} plate
 * @apiBody {String} mileage
 * @apiBody {String} observations
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {String} message
 * @apiSuccess {Object} data
 * @apiSuccess {Object} data.vehicle
 * @apiSuccess {Number} data.vehicle.id
 * @apiSuccess {String} data.vehicle.type
 * @apiSuccess {String} data.vehicle.model
 * @apiSuccess {String} data.vehicle.cylinder_capacity
 * @apiSuccess {String} data.vehicle.colour
 * @apiSuccess {String} data.vehicle.plate
 * @apiSuccess {String} data.vehicle.mileage
 * @apiSuccess {String} data.vehicle.status
 * @apiSuccess {String} data.vehicle.observations
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function store({ body }, res) {
  const response = await vehicleService.store({ ...body, status: 'ACTIVE' });

  Utils.executeResponse(res, response);
}

/**
 * @api {put} /vehicles/:vehicleId 4) Update Vehicle
 * @apiName UpdateVehicle
 * @apiGroup Vehicles
 *
 * @apiParam {Number} vehicleId
 *
 * @apiBody {String} type
 * @apiBody {String} model
 * @apiBody {String} cylinder_capacity
 * @apiBody {String} colour
 * @apiBody {String} plate
 * @apiBody {String} mileage
 * @apiBody {String} status
 * @apiBody {String} observations
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {String} message
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function update({ body, params: { vehicleId } }, res) {
  const response = await vehicleService.update(vehicleId, body);

  Utils.executeResponse(res, response);
}

/**
 * @api {delete} /vehicles/:vehicleId 5) Delete Vehicle By ID
 * @apiName DeleteVehicle
 * @apiGroup Vehicles
 *
 * @apiParam {Number} vehicleId Vehicle ID.
 *
 * @apiSuccess {Boolean} status
 * @apiSuccess {Striing} message
 *
 * @apiError (400) {Boolean} status
 * @apiError (400) {String} message
 * @apiError (400) {Object} error
 */
async function destroy({ params: { vehicleId } }, res) {
  const response = await vehicleService.delete(vehicleId);

  Utils.executeResponse(res, response);
}

module.exports = {
  vehicleService,
  find,
  index,
  store,
  update,
  destroy,
};
