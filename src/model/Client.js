class Client {
  id = null;
  document_number = '';
  name = '';
  email = '';
  birthdate = '';
  license = '';
  status = '';

  constructor({
    id = null,
    document_number = '',
    name = '',
    email = '',
    birthdate = '',
    license = '',
    status = '',
  }) {
    this.id = id;
    this.document_number = document_number;
    this.name = name;
    this.email = email;
    this.birthdate = birthdate;
    this.license = license;
    this.status = status;
  }
}

module.exports = Client;
