class ApiResponse {
  status = false;
  message = '';
  error = {};
  data = {};

  constructor({ status, message, data, error }) {
    this.status = status;
    this.message = message;
    this.data = data;
    this.error = error;
  }
}

module.exports = ApiResponse;
