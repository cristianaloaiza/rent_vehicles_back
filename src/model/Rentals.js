class Rentals {
  id = null;
  id_vehicle = 0;
  id_client = 0;
  id_price = 0;
  start_date = '';
  final_date = '';
  total = 0;
  status = '';
  observations = '';

  constructor({
    id = null,
    id_vehicle = 0,
    id_client = 0,
    id_price = 0,
    start_date = '',
    final_date = '',
    total = 0,
    status = '',
    observations = '',
  }) {
    this.id = id;
    this.id_vehicle = id_vehicle;
    this.id_client = id_client;
    this.id_price = id_price;
    this.start_date = start_date;
    this.final_date = final_date;
    this.total = total;
    this.status = status;
    this.observations = observations;
  }
}
module.exports = Rentals;
