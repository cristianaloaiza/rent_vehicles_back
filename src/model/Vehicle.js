class Vehicle {
  id = null;
  type = '';
  model = '';
  cylinder_capacity = '';
  colour = '';
  plate = '';
  mileage = '';
  status = '';
  observations = '';

  constructor({
    id = null,
    type = '',
    model = '',
    cylinder_capacity = '',
    colour = '',
    plate = '',
    mileage = '',
    status = '',
    observations = '',
  }) {
    this.id = id;
    this.type = type;
    this.model = model;
    this.cylinder_capacity = cylinder_capacity;
    this.colour = colour;
    this.plate = plate;
    this.mileage = mileage;
    this.status = status;
    this.observations = observations;
  }
}
module.exports = Vehicle;
