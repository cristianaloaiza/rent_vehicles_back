class Price {
  id = null;
  description = '';
  day_value = 0;
  vehicle_type = '';
  status = '';

  constructor({
    id = null,
    description = '',
    day_value = 0,
    vehicle_type = '',
    status = '',
  }) {
    this.id = id;
    this.description = description;
    this.day_value = day_value;
    this.vehicle_type = vehicle_type;
    this.status = status;
  }
}

module.exports = Price;
