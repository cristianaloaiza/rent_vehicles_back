class QueryResponse {
  status = false;
  message = '';
  response = {
    command: '',
    rowCount: 0,
    rows: [],
    fields: [],
  };
  error = {
    column: '',
    detail: '',
    name: '',
    schema: '',
    severity: '',
    table: '',
  };
  data = {};

  constructor({ status, message, data, response, error }) {
    this.status = status;
    this.message = message;
    this.response = response;
    this.data = data;
    this.error = error;
  }
}

module.exports = QueryResponse;
