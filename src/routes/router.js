const { Router } = require('express');

const router = Router();

const routes = {
  prefix: 'api',
  clients: 'clients',
  vehicles: 'vehicles',
  prices: 'prices',
  rentals: 'rentals',
};

router.get('/', (_, res) => {
  res.render('index', { title: 'Express' });
});

// Client
const ClientController = require('../controller/ClientController');

router.get(`/${routes.prefix}/${routes.clients}`, ClientController.index);
router.get(`/${routes.prefix}/${routes.clients}/:clientId`, ClientController.find);
router.post(`/${routes.prefix}/${routes.clients}`, ClientController.store);
router.put(`/${routes.prefix}/${routes.clients}/:clientId`, ClientController.update);
router.delete(`/${routes.prefix}/${routes.clients}/:clientId`, ClientController.destroy);

// Vehicle
const VehicleController = require('../controller/VehicleController');

router.get(`/${routes.prefix}/${routes.vehicles}`, VehicleController.index);
router.get(`/${routes.prefix}/${routes.vehicles}/:vehicleId`, VehicleController.find);
router.post(`/${routes.prefix}/${routes.vehicles}`, VehicleController.store);
router.put(`/${routes.prefix}/${routes.vehicles}/:vehicleId`, VehicleController.update);
router.delete(`/${routes.prefix}/${routes.vehicles}/:vehicleId`, VehicleController.destroy);
// Rentals
const RentalsController = require('../controller/RentalsController');

router.get(`/${routes.prefix}/${routes.rentals}`, RentalsController.index);
router.get(`/${routes.prefix}/${routes.rentals}/:rentalsId`, RentalsController.find);
router.post(`/${routes.prefix}/${routes.rentals}`, RentalsController.store);
router.put(`/${routes.prefix}/${routes.rentals}/:rentalsId`, RentalsController.update);
router.delete(`/${routes.prefix}/${routes.rentals}/:rentalsId`, RentalsController.destroy);

// Price
const PriceController = require('../controller/PriceController');

router.get(`/${routes.prefix}/${routes.prices}`, PriceController.index);
router.get(`/${routes.prefix}/${routes.prices}/:priceId`, PriceController.find);
router.post(`/${routes.prefix}/${routes.prices}`, PriceController.store);
router.put(`/${routes.prefix}/${routes.prices}/:priceId`, PriceController.update);
router.delete(`/${routes.prefix}/${routes.prices}/:priceId`, PriceController.destroy);

module.exports = router;
