const Client = require('../model/Client');
const QueryResponse = require('../model/QueryResponse');
const Utils = require('../utils/Utils');

class ClientRepository {
  utils = Utils;

  async all() {
    const res = await this.utils.query('SELECT * FROM clients');

    const response = new QueryResponse(res);

    if (response.status) {
      response.data = {
        clients: response.response.rows,
      };
    }

    return response;
  }

  async find(clientId) {
    const res = await this.utils.query('SELECT * FROM clients WHERE id = $1', [clientId]);

    const response = new QueryResponse(res);

    if (response.status && response.response.rowCount > 0) {
      response.data = {
        client: response.response.rows[0],
      };
    } else {
      response.status = false;
      response.message = 'Cliente no existe';
    }

    return response;
  }

  async store(client) {
    const _client = new Client(client);

    const res = await this.utils.query(
      'INSERT INTO clients (document_number, name, email, birthdate, license, status) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *',
      [
        _client.document_number,
        _client.name,
        _client.email,
        _client.birthdate,
        _client.license,
        _client.status,
      ],
    );

    const response = new QueryResponse(res);

    if (response.status && response.response.rowCount > 0) {
      response.message = 'Cliente creado correctamente';
      response.data = {
        client: response.response.rows[0],
      };
    } else {
      response.status = false;
      response.message = 'Ocurrió un error al crear el cliente';
    }

    return response;
  }

  async update(clientId, client) {
    const _client = new Client(client);

    const res = await this.utils.query(
      'UPDATE clients set document_number = $1, name = $2, email = $3, birthdate = $4, license = $5, status = $6 WHERE id = $7',
      [
        _client.document_number,
        _client.name,
        _client.email,
        _client.birthdate,
        _client.license,
        _client.status,
        clientId,
      ],
    );

    const response = new QueryResponse(res);

    if (response.status && response.response.rowCount > 0) {
      response.message = 'Cliente actualizado correctamente';
    } else {
      response.status = false;
      response.message = 'Ocurrió un error al actualizar el cliente';
    }

    return response;
  }

  async delete(clientId) {
    const res = await this.utils.query('DELETE FROM clients WHERE id = $1', [clientId]);

    const response = new QueryResponse(res);

    if (response.status && response.response.rowCount > 0) {
      response.message = 'Cliente eliminado correctamente';
    } else {
      response.status = false;
      response.message = 'Ocurrió un error al eliminar el cliente';
    }

    return response;
  }
}

module.exports = ClientRepository;
