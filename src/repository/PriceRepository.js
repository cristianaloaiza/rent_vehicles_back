const Price = require('../model/Price');
const QueryResponse = require('../model/QueryResponse');
const Utils = require('../utils/Utils');

class PriceRepository {
  utils = Utils;

  async all() {
    const res = await this.utils.query('SELECT * FROM prices');

    const response = new QueryResponse(res);

    if (response.status) {
      response.data = {
        prices: response.response.rows,
      };
    }

    return response;
  }

  async find(priceId) {
    const res = await this.utils.query('SELECT * FROM prices WHERE id = $1', [priceId]);

    const response = new QueryResponse(res);

    if (response.status && response.response.rowCount > 0) {
      response.data = {
        price: response.response.rows[0],
      };
    } else {
      response.status = false;
      response.message = 'Precio no existe';
    }

    return response;
  }

  async store(price) {
    const _price = new Price(price);

    const res = await this.utils.query(
      'INSERT INTO prices(description, day_value, vehicle_type, status) VALUES ($1, $2, $3, $4) RETURNING *',
      [_price.description, _price.day_value, _price.vehicle_type, _price.status],
    );

    const response = new QueryResponse(res);

    if (response.status && response.response.rowCount > 0) {
      response.message = 'Precio creado correctamente';
      response.data = {
        price: response.response.rows[0],
      };
    } else {
      response.status = false;
      response.message = 'Ocurrió un error al crear el precio';
    }

    return response;
  }

  async update(priceId, price) {
    const _price = new Price(price);

    const res = await this.utils.query(
      'UPDATE prices SET description = $1, day_value = $2, vehicle_type = $3, status = $4 WHERE id = $5',
      [_price.description, _price.day_value, _price.vehicle_type, _price.status, priceId],
    );

    const response = new QueryResponse(res);

    if (response.status && response.response.rowCount > 0) {
      response.message = 'Precio actualizado correctamente';
    } else {
      response.status = false;
      response.message = 'Ocurrió un error al actualizar el precio';
    }

    return response;
  }

  async delete(priceId) {
    const res = await this.utils.query('DELETE FROM prices WHERE id = $1', [priceId]);

    const response = new QueryResponse(res);

    if (response.status && response.response.rowCount > 0) {
      response.message = 'Precio eliminado correctamente';
    } else {
      response.status = false;
      response.message = 'Ocurrió un error al eliminar el precio';
    }

    return response;
  }
}

module.exports = PriceRepository;
