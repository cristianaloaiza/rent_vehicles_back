const QueryResponse = require('../model/QueryResponse');
const Rentals = require('../model/Rentals');
const Utils = require('../utils/Utils');

class RentalsRepository {
  utils = Utils;

  async all() {
    const res = await this.utils.query('SELECT * FROM rentals');

    const response = new QueryResponse(res);

    if (response.status) {
      response.data = {
        rentals: response.response.rows,
      };
    }

    return response;
  }

  async find(rentalsId) {
    const res = await this.utils.query('SELECT * FROM rentals WHERE id = $1', [rentalsId]);

    const response = new QueryResponse(res);

    if (response.status && response.response.rowCount > 0) {
      response.data = {
        rentals: response.response.rows[0],
      };
    } else {
      response.status = false;
      response.message = 'Alquiler no existe';
    }

    return response;
  }

  async store(rentals) {
    const _rentals = new Rentals(rentals);

    const res = await this.utils.query(
      'INSERT INTO rentals(id_vehicle, id_client, id_price, start_date, final_date, total, status, observations) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING *',
      [
        _rentals.id_vehicle,
        _rentals.id_client,
        _rentals.id_price,
        _rentals.start_date,
        _rentals.final_date,
        _rentals.total,
        _rentals.status,
        _rentals.observations,
      ],
    );

    const response = new QueryResponse(res);

    if (response.status && response.response.rowCount > 0) {
      response.message = 'Alquiler creado correctamente';
      response.data = {
        rentals: response.response.rows[0],
      };
    } else {
      response.status = false;
      response.message = 'Ocurrió un error al crear el alquiler';
    }

    return response;
  }

  async update(rentalsId, rentals) {
    const _rentals = new Rentals(rentals);

    const res = await this.utils.query(
      'UPDATE rentals SET id_vehicle = $1, id_client = $2, id_price = $3, start_date = $4, final_date = $5, total = $6, status = $7, observations =$8 WHERE id = $9',
      [
        _rentals.id_vehicle,
        _rentals.id_client,
        _rentals.id_price,
        _rentals.start_date,
        _rentals.final_date,
        _rentals.total,
        _rentals.status,
        _rentals.observations,
        rentalsId,
      ],
    );

    const response = new QueryResponse(res);

    if (response.status && response.response.rowCount > 0) {
      response.message = 'Alquiler actualizado correctamente';
    } else {
      response.status = false;
      response.message = 'Ocurrió un error al actualizar el alquiler';
    }
    return response;
  }

  async delete(rentalsId) {
    const res = await this.utils.query('DELETE FROM rentals WHERE id = $1', [rentalsId]);

    const response = new QueryResponse(res);

    if (response.status && response.response.rowCount > 0) {
      response.message = 'Alquiler eliminado correctamente';
    } else {
      response.status = false;
      response.message = 'Ocurrió un error al eliminar el alquiler';
    }
    return response;
  }
}

module.exports = RentalsRepository;
