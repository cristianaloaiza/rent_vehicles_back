const Vehicle = require('../model/Vehicle');
const QueryResponse = require('../model/QueryResponse');
const Utils = require('../utils/Utils');

class VehicleRepository {
  utils = Utils;

  async all() {
    const res = await this.utils.query('SELECT * FROM vehicles');

    const response = new QueryResponse(res);

    if (response.status) {
      response.data = {
        vehicles: response.response.rows,
      };
    }

    return response;
  }

  async find(vehicleId) {
    const res = await this.utils.query('SELECT * FROM vehicles WHERE id = $1', [vehicleId]);

    const response = new QueryResponse(res);

    if (response.status && response.response.rowCount > 0) {
      response.data = {
        vehicle: response.response.rows[0],
      };
    } else {
      response.status = false;
      response.message = 'Vehículo no existe';
    }

    return response;
  }

  async store(vehicle) {
    const _vehicle = new Vehicle(vehicle);

    const res = await this.utils.query(
      'INSERT INTO vehicles(type, model, cylinder_capacity, colour, plate, mileage, status, observations) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING *',
      [
        _vehicle.type,
        _vehicle.model,
        _vehicle.cylinder_capacity,
        _vehicle.colour,
        _vehicle.plate,
        _vehicle.mileage,
        _vehicle.status,
        _vehicle.observations,
      ],
    );

    const response = new QueryResponse(res);

    if (response.status && response.response.rowCount > 0) {
      response.message = 'Vehículo creado correctamente';
      response.data = {
        vehicle: response.response.rows[0],
      };
    } else {
      response.status = false;
      response.message = 'Ocurrió un error al crear el vehículo';
    }

    return response;
  }

  async update(vehicleId, vehicle) {
    const _vehicle = new Vehicle(vehicle);

    const res = await this.utils.query(
      'UPDATE vehicles SET type = $1, model = $2, cylinder_capacity = $3, colour = $4, plate = $5, mileage = $6, status = $7, observations =$8 WHERE id = $9',
      [
        _vehicle.type,
        _vehicle.model,
        _vehicle.cylinder_capacity,
        _vehicle.colour,
        _vehicle.plate,
        _vehicle.mileage,
        _vehicle.status,
        _vehicle.observations,
        vehicleId,
      ],
    );

    const response = new QueryResponse(res);

    if (response.status && response.response.rowCount > 0) {
      response.message = 'Vehículo actualizado correctamente';
    } else {
      response.status = false;
      response.message = 'Ocurrió un error al actualizar el vehículo';
    }
    return response;
  }

  async delete(vehicleId) {
    const res = await this.utils.query('DELETE FROM vehicles WHERE id = $1', [vehicleId]);

    const response = new QueryResponse(res);

    if (response.status && response.response.rowCount > 0) {
      response.message = 'Vehículo eliminado correctamente';
    } else {
      response.status = false;
      response.message = 'Ocurrió un error al eliminar el vehículo';
    }
    return response;
  }
}

module.exports = VehicleRepository;
