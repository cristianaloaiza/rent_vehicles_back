const pool = require('../../db/pool');
const ApiResponse = require('../model/ApiResponse');
const QueryResponse = require('../model/QueryResponse');

class Utils {
  static async query(sql, params) {
    let result = {};

    try {
      const client = await pool.connect();
      const response = await client.query(sql, params);
      client.release();

      result = { status: true, response };
    } catch (error) {
      result = {
        status: false,
        message: error.message,
        error,
      };
    }

    result = new QueryResponse(result);

    return result;
  }

  static executeResponse(res, response) {
    const _response = new ApiResponse(response);

    res.status(_response.status ? 200 : 400).json(_response);
  }
}

module.exports = Utils;
