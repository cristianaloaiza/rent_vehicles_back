const RentalsRepository = require('../repository/RentalsRepository');

class RentalsService {
  rentalsRepository = new RentalsRepository();

  all() {
    return this.rentalsRepository.all();
  }

  find(rentalsId) {
    return this.rentalsRepository.find(rentalsId);
  }

  store(rentals) {
    return this.rentalsRepository.store(rentals);
  }

  update(rentalsId, rentals) {
    return this.rentalsRepository.update(rentalsId, rentals);
  }

  delete(rentalsId) {
    return this.rentalsRepository.delete(rentalsId);
  }
}

module.exports = RentalsService;
