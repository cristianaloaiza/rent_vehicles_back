const ClientRepository = require('../repository/ClientRepository');

class ClientService {
  clientRepository = new ClientRepository();

  all() {
    return this.clientRepository.all();
  }

  find(clientId) {
    return this.clientRepository.find(clientId);
  }

  store(client) {
    return this.clientRepository.store(client);
  }

  update(clientId, client) {
    return this.clientRepository.update(clientId, client);
  }

  delete(clientId) {
    return this.clientRepository.delete(clientId);
  }
}

module.exports = ClientService;
