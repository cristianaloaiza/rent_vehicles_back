const PriceRepository = require('../repository/PriceRepository');

class PriceService {
  priceRepository = new PriceRepository();

  all() {
    return this.priceRepository.all();
  }

  find(priceId) {
    return this.priceRepository.find(priceId);
  }

  store(price) {
    return this.priceRepository.store(price);
  }

  update(priceId, price) {
    return this.priceRepository.update(priceId, price);
  }

  delete(priceId) {
    return this.priceRepository.delete(priceId);
  }
}

module.exports = PriceService;
