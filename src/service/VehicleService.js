const VehicleRepository = require('../repository/VehicleRepository');

class VehicleService {
  vehicleRepository = new VehicleRepository();

  all() {
    return this.vehicleRepository.all();
  }

  find(vehicleId) {
    return this.vehicleRepository.find(vehicleId);
  }

  store(vehicle) {
    return this.vehicleRepository.store(vehicle);
  }

  update(vehicleId, vehicle) {
    return this.vehicleRepository.update(vehicleId, vehicle);
  }

  delete(vehicleId) {
    return this.vehicleRepository.delete(vehicleId);
  }
}

module.exports = VehicleService;
