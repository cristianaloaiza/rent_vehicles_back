const { Pool } = require('pg');

const pool = new Pool({
  host: 'localhost',
  user: 'test_user',
  password: 'test_user_password',
  database: 'pipelines',
  port: '5432',
  idleTimeoutMillis: 0,
});

pool.query("SET SCHEMA 'rent_vehicles'");

module.exports = pool;
