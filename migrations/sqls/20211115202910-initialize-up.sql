CREATE SCHEMA IF NOT EXISTS rent_vehicles
    AUTHORIZATION test_user;

-- Clients

CREATE TABLE IF NOT EXISTS rent_vehicles.clients
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    document_number character varying(255) COLLATE pg_catalog."default" NOT NULL,
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    email character varying(255) COLLATE pg_catalog."default" NOT NULL,
    birthdate date NOT NULL,
    license character varying(255) COLLATE pg_catalog."default" NOT NULL,
    status character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT clients_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE rent_vehicles.clients
    OWNER to test_user;
	
GRANT ALL ON TABLE rent_vehicles.clients TO test_user;

-- Vehicles

CREATE TABLE IF NOT EXISTS rent_vehicles.vehicles
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    type character varying(255) COLLATE pg_catalog."default" NOT NULL,
    model character varying(255) COLLATE pg_catalog."default" NOT NULL,
    cylinder_capacity character varying(255) COLLATE pg_catalog."default" NOT NULL,
    colour character varying(255) COLLATE pg_catalog."default" NOT NULL,
    plate character varying(255) COLLATE pg_catalog."default" NOT NULL,
    mileage character varying(255) COLLATE pg_catalog."default" NOT NULL,
    status character varying(255) COLLATE pg_catalog."default" NOT NULL,
    observations character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT vehicles_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE rent_vehicles.vehicles
    OWNER to test_user;

GRANT ALL ON TABLE rent_vehicles.vehicles TO test_user;

-- Prices

CREATE TABLE IF NOT EXISTS rent_vehicles.prices
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    description character varying(255) COLLATE pg_catalog."default" NOT NULL,
    day_value double precision NOT NULL,
    vehicle_type character varying(255) COLLATE pg_catalog."default" NOT NULL,
    status character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT prices_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE rent_vehicles.prices
    OWNER to test_user;

GRANT ALL ON TABLE rent_vehicles.prices TO test_user;

-- Rentals

CREATE TABLE IF NOT EXISTS rent_vehicles.rentals
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    id_vehicle integer NOT NULL,
    id_client integer NOT NULL,
    id_price integer NOT NULL,
    start_date date NOT NULL,
    final_date date NOT NULL,
    total double precision NOT NULL,
    status character varying(255) COLLATE pg_catalog."default" NOT NULL,
    observations character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT rentals_pkey PRIMARY KEY (id),
    CONSTRAINT clients FOREIGN KEY (id_client)
        REFERENCES rent_vehicles.clients (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT prices FOREIGN KEY (id_price)
        REFERENCES rent_vehicles.prices (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT vehicles FOREIGN KEY (id_vehicle)
        REFERENCES rent_vehicles.vehicles (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE rent_vehicles.rentals
    OWNER to test_user;

GRANT ALL ON TABLE rent_vehicles.rentals TO test_user;